use jni::{
    JNIEnv,
    objects::{
        JClass,
    },
    sys::{
        jbyteArray,
        jlong,
    },
};

use wasmer_runtime::{
    memory::{
        Memory,
        MemoryView,
    },
    units::{
        Pages,
    },
    wasm::{
        MemoryDescriptor,
    }
};

use snafu::{Snafu, ResultExt};

#[derive(Debug, Snafu)]
enum Error {
    #[snafu(display("could not create wasm instance: {}", source))]
    InstanceError { source: wasmer_runtime::error::Error },
    #[snafu(display("could not create wasm memory: {}", source))]
    MemoryError { source: wasmer_runtime_core::error::CreationError },
}

fn host_load(
    _ctx: &mut wasmer_runtime::Ctx,
    mem_idx: u32,
    wasm_ptr: u32, 
    wasm_len: u32)
-> u32 {
    let mem_view: MemoryView<u8> = _ctx.memory(mem_idx).view();
    let wasm: Vec<_> = mem_view
        .get((wasm_ptr as usize)..((wasm_ptr + wasm_len) as usize))
        .unwrap()
        .iter()
        .map(|cell| cell.get())
        .collect();
    0
}

struct WasmInstance {
    pub inner: wasmer_runtime::Instance,
}

impl WasmInstance {
    fn new(wasm: &[u8]) -> Result<WasmInstance, Error> {
        let shared_memory = Memory::new(
            MemoryDescriptor {
                minimum: Pages(1), // TODO: Make this configurable
                maximum: Some(Pages(1)), // TODO: Make this configurable
                shared: true,
            }
        ).context(MemoryError)?;

        let mut import_object = wasmer_runtime::imports! {
            "env" => {
                "load" => wasmer_runtime::func!(host_load),
            },
        };

        import_object.extend(
            vec![("env".into(), "memory".into(), wasmer_runtime::Export::Memory(shared_memory))]);

        let instance_inner = wasmer_runtime::instantiate(wasm, &import_object)
            .context(InstanceError)?;

        let instance = WasmInstance {
            inner: instance_inner,
        };

        Ok(instance)
    }
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "system" fn Java_WasmInstance_create(
    _env: JNIEnv,
    _class: JClass,
    wasmBytes: jbyteArray
) -> jlong {
    let wasm = _env.convert_byte_array(wasmBytes).unwrap();
    
    match WasmInstance::new(&wasm) {
        Ok(res) => Box::into_raw(Box::new(res)) as jlong,
        Err(err) => {
            _env.throw(format!("{}", err)).expect("could not throw error");
            0
        }
    }
}

#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "system" fn Java_WasmInstance_destroy(
    _env: JNIEnv,
    _class: JClass,
    instance_ptr: jlong
) {
    let instance = instance_ptr as *mut WasmInstance;
    std::mem::drop(Box::from_raw(instance));
}
