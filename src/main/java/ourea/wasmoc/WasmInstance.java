package ourea.wasmoc;

public class WasmInstance {
	private static native long create(byte[] wasm);

	private static native void destroy(long pointer);

	static {
		System.loadLibrary("runtime");
	}
}
